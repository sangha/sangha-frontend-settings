# donate.techcultivation.org
Open finances for open projects

## You do the hard work, we handle your bookkeeping chores
Maintaining an active open source project and its community is hard work. You are already putting in all the extra hours and users love your project. They would totally support you with a donation every now and then, but you have neither the infrastructure to accept the donations nor the time for dealing with all that bureaucratic jibber-jabber. Filling out one
tax form per year is enough, right?

The Center for the Cultivation of Technology understands the dilemma and enables open source projects to quickly set up the infrastructure to accept donations, raise funds and manage budgets transparently.

- Donors receive a proper, tax-deductible receipt
- Let users directly support a cause by creating specific budgets (e.g. for a developer meeting)
- Transparent bookkeeping for transparent projects

Have an open source project? Apply to join our platform!

## Open Source projects you can already support
These awesome [projects](/projects) have joined our platform. Chip in and support them with a donation!

<div>
  <sangha-top-projects
    base-url="/projects"
    limit="4"
  ></sangha-top-projects>
</div>
