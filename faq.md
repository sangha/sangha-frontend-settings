# FAQ

## The platform

### What do you spend your money on?

### What is Sangha?
Sangha is the software platform that this site is running on. It's open source and you can install and host it yourself, too.

---

## For Projects

### How can I sign up?
We are currently in a public beta test and welcome new projects to our platform on an invite-only basis. Apply with your open source project now
and we'll get in touch with you!

### How much does it cost?
The Center takes 10% of the money raised by a project for managing bookkeeping, hosting the software and furthering development of the Sangha platform.

Additionally our payment processors may charge a fee for receiving money (e.g. Credit Card fees) and paying out expenses.

### What happens to the money if I close my account?

---

## For Users

### How do I make a donation?
Go to a project's public page, choose a donation amount and hit the Donate button. Fill out the form if you want a donation receipt and don't forget to give yourself a pat on the shoulder! Thanks for supporting open source development, you rock!
