# Sangha settings

Some JSON and Markdown files or better known as the worlds simplest CMS.

## Run with docker-compose

```yaml
version: '2'

services:
  sangha-settings:
    image: registry.gitlab.com/sangha/sangha-frontend-settings
    ports:
      - 8080:80
    environment:
      - ENVIRONMENT=sandbox
```
